# Car Insurance

This is a simple dApp use to manage car insurances. Users interface is connected with blockchain technology.

## Getting Started

### Prerequisites

* Node.js - http-server module to deploy application
* Truffle - framework to developing, testing and starting dapp projects
* Ganache-CLI - TestRPC, customizable blockchain emulator
* MetaMask - extension for Chrome browser to manage ethereum wallet
 


### Installing

1. Install Truffle: https://www.npmjs.com/package/truffle
2. In main directory project use command: `$ truffle compile`
3. Start ganache client (command line: $ ganache-cli -p 7545)
4. Now you can migrate smart contact (dapp) to blockchain, in project directory start `$ truffle migrate --network development`. Ganache generates 10 wallets keys (100 eth), you can import them to MetaMask.
5. Start your http server: `http-server "path/to/dapp"


Browser access to application is *http://127.0.0.1:8080* (default). Go to */app* directory and testing car insurance system.

