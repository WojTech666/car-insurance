pragma solidity ^0.4.0;

contract CarInsurance {
    uint insuranceCost;
    uint insuranceValue;
    address agency;

    constructor() public {
        agency = msg.sender;
        insuranceValue = 10 ether;
        insuranceCost = 10 ether;
    }

    modifier onlyAgency() {
        require(msg.sender == agency);
        _;
    }

    struct Insurance {
        uint id;
        uint carCounter;
        uint insuranceValue;
        string owner;
    }

    //id of owners = id insurance
    mapping(address => Insurance) ownersInsurances;
    address[] owners;

    function getInsuranceData(address _owner) public view returns(string, uint, uint) {
        Insurance memory item = ownersInsurances[_owner];
        return (item.owner, item.carCounter, item.insuranceValue);
    }

    function addUser(string _username, address _user) public onlyAgency {
        require(ownersInsurances[_user].id == 0);
        uint id = owners.length + 1;
        ownersInsurances[_user] = Insurance(id, 0, 0, _username);
        owners.push(_user);
    }

    function buyInsurance(address _buyer) public payable {
        require(_buyer.balance >= insuranceCost);
        ownersInsurances[_buyer].insuranceValue += insuranceValue;
    }

    function addCar(address _owner) public payable {
        require(_owner.balance >= insuranceCost);
        ownersInsurances[_owner].carCounter += 1;
    }

    function reportDamage(uint _damage, address _owner) public {
            _owner.transfer(_damage);
    }

    function getAgency() public view returns (bool) {
        return msg.sender == agency;
    }
}