var contractAddress = "0x7001052eea2e205266235ad40b602c373e2af954";
var contractABI = JSON.parse(contractAbi);
var contract = web3.eth.contract(contractABI).at(contractAddress);

window.addEventListener('load', function() {

    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
    }

    app();
})

function app() {
    checkAgency();

    $("#add-user").on('click', function() {
        addUser();
    });

    $("#check").on('click', function() {
        checkInsurance();
    });

    $("#buy-ins").on('click', function() {
        buyInsurance();
    });

    $("#report-damage").on('click', function() {
        reportDamage();
    });
}

function checkAgency() {
    console.log("init");
    setInterval(function() {
        currentAddress = web3.eth.accounts[0];
        contract.getAgency(function(error, result) {
            if(result) {
                $("#user-name").removeAttr('disabled');
                $("#user-address").removeAttr('disabled');
            } else {
                $("#user-name").attr('disabled', 'disabled');
                $("#user-address").attr('disabled', 'disabled');
            }
        });
    }, 2000);
}

function checkInsurance() {
    address = web3.eth.accounts[0];

    contract.getInsuranceData(address, function(error, result){
        console.log(result);
        renderData(result);
    });
}

function addUser() {
    username = $("#user-name").val();
    userAddress = $("#user-address").val();

    contract.addUser(username, userAddress, function(error, result){
        if(!error) {
            showInfo("Add user to agency");
        } else {
            showInfo("Error occurred");
        }
    });
}

function buyInsurance() {
    userAddress = web3.eth.accounts[0];

    contract.buyInsurance(userAddress, {value:10000000000000000000}, function(error, result){
        if(!error) {
            showInfo("You bought insurance");
        } else {
            showInfo("Error occurred");
        }
    });
}

function reportDamage() {
    address = web3.eth.accounts[0];
    opts = $("#level input");

    level = 1;

    opts.each(function() {
        if($(this).attr('checked') == 'checked') {
            level = parseInt($(this).val());
        }
    });
    alert(level);
    contract.reportDamage(5000000000000000000, address, function(error, result){
        if(!error) {
            showInfo("Damage reported");
        } else {
            showInfo("Error occurred");
        }
    });
}

function renderData(data) {
    $('tbody tr').each(function(index) {
        $(this).find('td').last().text(data[index]);
    });
}

function showInfo(message) {
    info = $("#snackbar");
    info.text(message);
    info.addClass("show");
    setTimeout(function(){
        info.removeClass("show");
    }, 3000);
}
